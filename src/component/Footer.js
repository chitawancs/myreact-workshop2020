import React from "react";

const Footer = (props) => {
  const { company, email } = props;
  return (
    <div className="fixed-bottom">
      <div className="container-fluid">
        <div className="text-center title text-uppercase">
          <small>
            <span className="text-danger"> Powerd By {company} </span>
            <span className="text-muted">
              | Contact By Email : {email} <br />
              Copyright © 2020
            </span>
          </small>
        </div>
      </div>
    </div>
  );
};

export default Footer;
