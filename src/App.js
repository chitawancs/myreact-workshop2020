import "./App.css";
import React from "react";
import Header from "./component/Header";
import Footer from "./component/Footer";
import Productitem from "./component/product/Productitem";

function App() {
  return (
    <div className="App">
      <Header></Header>
      <Productitem productName ="I Phone 12" unitPrice="25,000"></Productitem>
      <Footer company="Chitawan" email="chitawan.info@email.com"></Footer>
    </div>
  );
}

export default App;
